import { Component, OnInit } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';
import { RegisterComponent } from '../register/register.component';
//import { NavigationEnd, Router } from '@angular/router';
import { UserService } from '../user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userdetails : any;
  email : any;
  password : any;
  c: any;

  constructor(private hhtpClient : HttpClient,private service :UserService, private router:Router) {
    this.email = '';
    this.password = '';
  }
  ngOnInit(): void {
      this.service.showAllUsers().subscribe((result:any) => {
        this.userdetails = result;
      });

    }
    signIn(LoginForm:any){
      this.userdetails.forEach((u:any) => {
        if(u.email === LoginForm.email && u.password === LoginForm.password){
          this.c = 1;
          alert("login succesfull");
          this.router.navigate(['/viewheader']);
        }
      });
      if(this.c != 1){alert("INVALID LOGIN");}

    }

  }


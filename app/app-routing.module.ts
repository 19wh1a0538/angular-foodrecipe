import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
//import { loginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { FoodpageComponent } from './foodpage/foodpage.component';
import { CartPageComponent } from './cart-page/cart-page.component';
import { ViewheaderComponent } from './viewheader/viewheader.component';



const routes: Routes = [
  {path:"",component:LoginComponent},
  {path:'login',component: LoginComponent },
  {path:'register',component: RegisterComponent},
  {path:'home',component:HomeComponent},
  {path:'search/:searchItem',component:HomeComponent},
  //{path :' ',component:HomeComponent},
  {path:'tag/:tag',component:HomeComponent},
  {path:'food/:id',component:FoodpageComponent},
  {path: 'cart-page',component:CartPageComponent},
  {path:'viewheader',component:ViewheaderComponent}

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

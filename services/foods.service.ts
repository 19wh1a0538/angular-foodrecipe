import { Injectable } from '@angular/core';
import { Foods } from 'src/app/shared/models/food';
import {Tag} from 'src/app/shared/models/tag';

@Injectable({
  providedIn: 'root'
})
export class FoodsService {

  constructor() { }
  getFoodById (id:number):Foods{
    return this.getAll().find(food => food.id  == id)!; 
  }
  getAllFoodByTag(tag:string):Foods[]{
      return tag == "All"?
      this.getAll(): this.getAll().filter(food => food.tags?.includes(tag));
  }
  
 getAllTag():Tag[]{
  return[
    { name: 'All' , count:36},
    { name: 'Salads' , count: 4},
    { name: 'Dairy Free Products' , count:4},
    { name: 'Desserts' , count:4},
    { name: 'Rice' , count:4},
    { name: 'Fruit Juices' , count:4},
    { name: 'Appetizers',count:4},
    { name: 'Low Calorie Food',count:4},
    { name: 'Sprouts Recipes', count:4},
    { name: 'Soups', count:4}


  ];}
  getAll():Foods[]{
    return[
    {
    
      id :1,
      name :'Cabbage salad',
      favourite :false,
      star :5,
      imageUrl :'/assets/imagess/b.jpeg',
      cooktime : '10(min)',
      tags :['Salads'],
      description : '𝐓𝐡𝐞 𝐟𝐚𝐛𝐮𝐥𝐨𝐮𝐬 𝐜𝐚𝐛𝐛𝐚𝐠𝐞 𝐬𝐚𝐥𝐚𝐝 𝐫𝐞𝐜𝐢𝐩𝐞 𝐢𝐬 𝐚 𝐪𝐮𝐢𝐜𝐤 𝐜𝐚𝐛𝐛𝐚𝐠𝐞 𝐬𝐚𝐥𝐚𝐝 𝐰𝐡𝐢𝐜𝐡 𝐢𝐬 𝐚𝐥𝐬𝐨 𝐡𝐞𝐚𝐥𝐭𝐡𝐲. 𝐂𝐚𝐛𝐛𝐚𝐠𝐞 𝐡𝐚𝐬 𝐡𝐢𝐠𝐡 𝐥𝐞𝐯𝐞𝐥𝐬 𝐨𝐟 𝐟𝐥𝐚𝐯𝐨𝐧𝐨𝐢𝐝𝐬 𝐚𝐧𝐝 𝐚𝐧𝐭𝐡𝐨𝐜𝐲𝐚𝐧𝐢𝐧𝐬 𝐚𝐧𝐝 𝐡𝐚𝐬 𝐥𝐨𝐧𝐠 𝐛𝐞𝐞𝐧 𝐮𝐬𝐞𝐝 𝐚𝐬 𝐚 𝐡𝐞𝐫𝐛𝐚𝐥 𝐦𝐞𝐝𝐢𝐜𝐢𝐧𝐞.',
      ImageUrl:'/assets/imagess/CabbageSalad.png',
      Price:0
      

    },
    {
      id :2,
      name :'Strawberry Lime Smoothie',
      favourite :false,
      star :5,
      imageUrl :'/assets/imagess/c.jpeg',
      cooktime : '10-15(min)',
      tags : ['Fruit Juices'],
      description : '𝐈𝐭 𝐢𝐬 𝐠𝐨𝐨𝐝 𝐟𝐨𝐫 𝐲𝐨𝐮 𝐛𝐞𝐜𝐚𝐮𝐬𝐞 𝐬𝐭𝐫𝐚𝐰𝐛𝐞𝐫𝐫𝐢𝐞𝐬 𝐚𝐫𝐞 𝐫𝐢𝐜𝐡 𝐢𝐧 𝐟𝐢𝐛𝐞𝐫 ,𝐯𝐢𝐭𝐚𝐦𝐢𝐧𝐬 𝐚𝐧𝐝 𝐚𝐧𝐭𝐢-𝐨𝐱𝐢𝐝𝐚𝐧𝐭𝐬 .𝐓𝐡𝐞𝐲 𝐚𝐫𝐞 𝐚𝐦𝐨𝐧𝐠 𝐭𝐡𝐞 𝐭𝐨𝐩 𝟐𝟎 𝐦𝐨𝐬𝐭 𝐚𝐧𝐭𝐢-𝐨𝐱𝐢𝐝𝐚𝐧𝐭𝐬 𝐫𝐢𝐜𝐡 𝐟𝐫𝐮𝐢𝐭𝐬 .𝐎𝐧𝐞 𝐬𝐞𝐫𝐯𝐢𝐧𝐠 𝐨𝐟 𝐬𝐭𝐫𝐚𝐰𝐛𝐞𝐫𝐫𝐢𝐞𝐬 𝐜𝐨𝐧𝐭𝐚𝐢𝐧𝐬 𝐦𝐨𝐫𝐞 𝐯𝐢𝐭𝐚𝐦𝐢𝐧 𝐂 𝐭𝐡𝐚𝐧 𝐨𝐫𝐚𝐧𝐠𝐞.',
      ImageUrl:'/assets/imagess/StrawberryLimeSmoothie.png',
      Price:0
    },
    {
      id :3,
      name :'Bajra roti',
      favourite :false,
      star :4.2,
      imageUrl :'/assets/imagess/d.jpeg',
      cooktime : '5-10(min)',
      tags : ['Appetizers'],
      description : '𝐁𝐚𝐣𝐫𝐚 𝐡𝐚𝐬 𝐛𝐞𝐢𝐧𝐠 𝐥𝐢𝐧𝐤𝐞𝐝 𝐭𝐨 𝐬𝐢𝐠𝐧𝐢𝐟𝐢𝐜𝐚𝐧𝐭 𝐡𝐞𝐚𝐥𝐭𝐡 𝐛𝐞𝐧𝐞𝐟𝐢𝐭𝐬 𝐬𝐢𝐦𝐩𝐥𝐲 𝐝𝐮𝐞 𝐭𝐨 𝐢𝐭𝐬 𝐬𝐭𝐚𝐭𝐮𝐬 𝐡𝐚𝐬 𝐚 𝐰𝐡𝐨𝐥𝐞 𝐠𝐫𝐚𝐢𝐧 𝐟𝐨𝐨𝐝. 𝐑𝐞𝐠𝐮𝐥𝐚𝐫𝐥𝐲 𝐞𝐚𝐭𝐢𝐧𝐠 𝐁𝐚𝐣𝐫𝐚 𝐫𝐨𝐭𝐢 𝐦𝐚𝐲 𝐡𝐞𝐥𝐩 𝐩𝐫𝐞𝐯𝐞𝐧𝐭 𝐜𝐡𝐫𝐨𝐧𝐢𝐜 𝐜𝐨𝐧𝐝𝐢𝐭𝐢𝐨𝐧𝐬 𝐥𝐢𝐤𝐞 𝐃𝐢𝐚𝐛𝐞𝐭𝐞𝐬, 𝐡𝐞𝐚𝐫𝐭 𝐝𝐢𝐬𝐞𝐚𝐬𝐞𝐬 𝐚𝐧𝐝 𝐜𝐞𝐫𝐭𝐚𝐢𝐧 𝐜𝐚𝐧𝐜𝐞𝐫𝐬.',
      ImageUrl:'/assets/imagess/BajraRoti.png',
      Price:0
    },
    {
      id :4,
      name :"Raddish koftha",
      favourite :true,
      star :4.9,
      imageUrl :'/assets/imagess/e.jpeg',
      cooktime : '6-8(min)',
      tags : ['Appetizers'],
      description : '𝐊𝐨𝐟𝐭𝐚𝐬 𝐚𝐫𝐞 𝐚 𝐝𝐞𝐥𝐢𝐜𝐢𝐨𝐮𝐬 𝐫𝐞𝐜𝐢𝐩𝐞 𝐭𝐡𝐚𝐭 𝐞𝐯𝐞𝐫𝐲𝐛𝐨𝐝𝐲 𝐩𝐫𝐞𝐟𝐞𝐫𝐬 𝐭𝐨 𝐜𝐨𝐨𝐤 𝐨𝐧 𝐚 𝐬𝐩𝐞𝐜𝐢𝐚𝐥 𝐝𝐚𝐲. 𝐓𝐡𝐞 𝐝𝐞𝐥𝐞𝐜𝐭𝐚𝐛𝐥𝐞 𝐫𝐚𝐰 𝐛𝐚𝐧𝐚𝐧𝐚 𝐚𝐧𝐝 𝐫𝐚𝐝𝐢𝐬𝐡 𝐤𝐨𝐟𝐭𝐚𝐬 𝐚𝐫𝐞 𝐧𝐨𝐭 𝐟𝐫𝐢𝐞𝐝 𝐛𝐮𝐭 𝐬𝐡𝐚𝐥𝐥𝐨𝐰 𝐟𝐫𝐢𝐞𝐝 𝐮𝐬𝐢𝐧𝐠 𝐚 𝐩𝐚𝐧 , 𝐦𝐚𝐤𝐢𝐧𝐠 𝐢𝐭 𝐚 𝐡𝐞𝐚𝐥𝐭𝐡𝐲 𝐜𝐡𝐨𝐢𝐜𝐞.',
      ImageUrl:'/assets/imagess/RadishKofta.png',
      Price:0

    },
    {
      id :5,
      name :'Ginger Cinnamon tea',
     
      favourite :true,
      star :4.6,
      imageUrl :'/assets/imagess/f.jpeg',
      cooktime:'10-12(min)',
      tags : ['Low Calorie Food'],
      description : '𝐆𝐢𝐧𝐠𝐞𝐫 𝐜𝐢𝐧𝐧𝐚𝐦𝐨𝐧 𝐭𝐞𝐚 𝐢𝐬 𝐚 𝐜𝐥𝐞𝐚𝐧𝐬𝐢𝐧𝐠 𝐝𝐫𝐢𝐧𝐤. 𝐓𝐡𝐢𝐬 𝐡𝐞𝐚𝐥𝐭𝐡𝐲 𝐠𝐢𝐧𝐠𝐞𝐫 𝐭𝐞𝐚 𝐟𝐨𝐫 𝐜𝐨𝐥𝐝 𝐢𝐬 𝐬𝐮𝐫𝐞 𝐭𝐨 𝐩𝐮𝐭 𝐲𝐨𝐮 𝐛𝐚𝐜𝐤 𝐨𝐧 𝐭𝐡𝐞 𝐫𝐢𝐠𝐡𝐭 𝐭𝐫𝐚𝐜𝐤 𝐨𝐧 𝐚𝐧 𝐚𝐢𝐥𝐢𝐧𝐠 𝐝𝐚𝐲 . 𝐈𝐭 𝐢𝐬 𝐚𝐧 𝐢𝐦𝐩𝐫𝐨𝐯𝐢𝐬𝐞𝐝 𝐢𝐦𝐦𝐮𝐧𝐞 𝐛𝐨𝐨𝐬𝐭𝐞𝐫 𝐰𝐢𝐭𝐡 𝐭𝐡𝐞 𝐟𝐢𝐧𝐞 𝐟𝐥𝐚𝐯𝐨𝐮𝐫 𝐨𝐟 𝐜𝐢𝐧𝐧𝐚𝐦𝐨𝐧 𝐚𝐧𝐝 𝐭𝐡𝐞 𝐩𝐥𝐞𝐚𝐬𝐚𝐧𝐭 𝐬𝐰𝐞𝐞𝐭𝐧𝐞𝐬𝐬 𝐨𝐟 𝐡𝐨𝐧𝐞𝐲.',
      ImageUrl:'/assets/imagess/GingerCinnamonTea.png',
      Price:0

    },
    {
      id :6,
      name :"Indian Tomato Soup",
      favourite :true,
      star :4.5,
      imageUrl :'/assets/imagess/g.jpeg',
      cooktime : '4-6(min)',
      tags : ['Soups'],
      description : '𝐓𝐡𝐢𝐬 𝐞𝐚𝐬𝐲 𝐭𝐨𝐦𝐚𝐭𝐨 𝐬𝐨𝐮𝐩 𝐫𝐞𝐜𝐢𝐩𝐞 𝐦𝐚𝐤𝐞𝐬 𝐚 𝐜𝐫𝐞𝐚𝐦𝐲, 𝐫𝐢𝐜𝐡 𝐭𝐨𝐦𝐚𝐭𝐨 𝐬𝐨𝐮𝐩 𝐭𝐡𝐚𝐭𝐬 𝐩𝐞𝐫𝐟𝐞𝐜𝐭 𝐟𝐨𝐫 𝐞𝐧𝐣𝐨𝐲𝐢𝐧𝐠 𝐚𝐬 𝐚 𝐡𝐞𝐚𝐫𝐭𝐲 𝐥𝐮𝐧𝐜𝐡 𝐨𝐫 𝐥𝐢𝐠𝐡𝐭 𝐝𝐢𝐧𝐧𝐞𝐫. 𝐙𝐞𝐬𝐭𝐲 𝐟𝐫𝐞𝐬𝐡 𝐭𝐨𝐦𝐚𝐭𝐨𝐞𝐬 𝐚𝐧𝐝 𝐫𝐞𝐚𝐥 𝐜𝐫𝐞𝐚𝐦 𝐜𝐫𝐞𝐚𝐭𝐞 𝐚 𝐭𝐫𝐮𝐥𝐲 𝐜𝐨𝐦𝐟𝐨𝐫𝐭𝐢𝐧𝐠 , 𝐯𝐞𝐥𝐯𝐞𝐭𝐲 𝐬𝐦𝐨𝐨𝐭𝐡 𝐭𝐨𝐦𝐚𝐭𝐨 𝐬𝐨𝐮𝐩 𝐟𝐫𝐨𝐦 𝐬𝐜𝐫𝐚𝐭𝐜𝐡 -𝐢𝐧 𝐥𝐞𝐬𝐬 𝐭𝐡𝐚𝐧 𝟑𝟎 𝐦𝐢𝐧.',
      ImageUrl:'/assets/imagess/IndianTomatoSoup.png',
      Price:0

    },
    {
      id :7,
      name :'Veg upma',
      favourite :false,
      star :4.7,
      imageUrl :'/assets/imagess/h.jpeg',
      cooktime : '5-8(min)',
      tags : ['Appetizers'],
      description : '𝐔𝐩𝐦𝐚 𝐢𝐬 𝐚 𝐟𝐥𝐚𝐯𝐨𝐫𝐟𝐮𝐥 𝐬𝐨𝐮𝐭𝐡 𝐈𝐧𝐝𝐢𝐚𝐧 𝐛𝐫𝐞𝐚𝐤𝐟𝐚𝐬𝐭 𝐝𝐢𝐬𝐡 𝐦𝐚𝐝𝐞 𝐟𝐫𝐨𝐦 𝐫𝐚𝐯𝐚 𝐨𝐫 𝐜𝐫𝐞𝐚𝐦 𝐨𝐟 𝐰𝐡𝐞𝐚𝐭 . 𝐀 𝐛𝐨𝐰𝐥 𝐨𝐟 𝐮𝐩𝐦𝐚 𝐡𝐚𝐬 𝐟𝐢𝐛𝐞𝐫 ,𝐯𝐢𝐭𝐚𝐦𝐢𝐧𝐬 ,𝐡𝐞𝐚𝐥𝐭𝐡𝐲 𝐟𝐚𝐭𝐬 .𝐈𝐭 𝐢𝐬 𝐥𝐨𝐰 𝐢𝐧 𝐜𝐡𝐨𝐥𝐞𝐬𝐭𝐞𝐫𝐨𝐥 𝐚𝐧𝐝 𝐜𝐚𝐥𝐨𝐫𝐢𝐞𝐬 𝐰𝐡𝐢𝐜𝐡 𝐦𝐚𝐤𝐞𝐬 𝐢𝐭 𝐚 𝐡𝐞𝐚𝐥𝐭𝐡𝐲 𝐦𝐞𝐚𝐥 𝐚𝐧𝐝 𝐡𝐞𝐥𝐩𝐬 𝐲𝐨𝐮 𝐡𝐚𝐯𝐞 𝐚 𝐛𝐚𝐥𝐚𝐧𝐜𝐞𝐝 𝐝𝐢𝐞𝐭.',
      ImageUrl:'/assets/imagess/VegUpma.png',
      Price:0
      

    },
    {
      id :8,
      name :"Ginger Garlic Soup",
      favourite :true,
      star :5,
      imageUrl :'/assets/imagess/I.jpeg',
      cooktime : '15(min)',
      tags :['Soups'],
      description : '𝐒𝐮𝐥𝐩𝐡𝐮𝐫 𝐜𝐨𝐧𝐭𝐚𝐢𝐧𝐢𝐧𝐠 𝐜𝐨𝐦𝐩𝐨𝐮𝐧𝐝𝐬 𝐢𝐧𝐜𝐥𝐮𝐝𝐢𝐧𝐠 𝐀𝐥𝐥𝐢𝐜𝐢𝐧 ,𝐰𝐡𝐢𝐜𝐡 𝐠𝐢𝐯𝐞 𝐠𝐚𝐫𝐥𝐢𝐜 𝐢𝐭𝐬 𝐩𝐮𝐧𝐠𝐞𝐧𝐜𝐲 ,𝐚𝐥𝐨𝐧𝐠 𝐰𝐢𝐭𝐡 𝐡𝐢𝐠𝐡 𝐥𝐞𝐯𝐞𝐥𝐬 𝐨𝐟 𝐯𝐢𝐭𝐚𝐦𝐢𝐧 𝐂, 𝐯𝐢𝐭𝐚𝐦𝐢𝐧 𝐁𝟔,𝐌𝐚𝐧𝐠𝐚𝐧𝐞𝐬𝐞 𝐚𝐧𝐝 𝐬𝐞𝐥𝐞𝐧𝐢𝐮𝐦 𝐬𝐞𝐞𝐦 𝐭𝐨 𝐛𝐞 𝐫𝐞𝐬𝐩𝐨𝐧𝐬𝐢𝐛𝐥𝐞 𝐟𝐨𝐫 𝐠𝐚𝐫𝐥𝐢𝐜𝐬 𝐛𝐞𝐧𝐞𝐟𝐢𝐜𝐢𝐚𝐥 𝐜𝐚𝐫𝐝𝐢𝐨 𝐯𝐚𝐬𝐜𝐮𝐥𝐚𝐫 𝐞𝐟𝐟𝐞𝐜𝐭𝐬 .',
      ImageUrl:'/assets/imagess/GingerGarlicSoup.png',
      Price:0
    

    },
    {
      id :9,
      name :"Corn Salsa",
      favourite :true,
      star :4.5,
      imageUrl :'/assets/imagess/J.jpeg',
      cooktime : '4-6(min)',
      tags :['Salads'],
      description : '𝐓𝐡𝐞 𝐛𝐞𝐬𝐭 𝐜𝐨𝐫𝐧 𝐬𝐚𝐥𝐬𝐚 𝐢𝐬 𝐞𝐚𝐬𝐲 𝐭𝐨 𝐦𝐚𝐤𝐞 𝐟𝐫𝐨𝐦 𝐬𝐜𝐫𝐚𝐭𝐜𝐡 𝐰𝐢𝐭𝐡 𝐟𝐫𝐞𝐬𝐡 𝐬𝐰𝐞𝐞𝐭 𝐜𝐨𝐫𝐧 𝐚𝐧𝐝 𝐨𝐧𝐥𝐲 𝐚 𝐟𝐞𝐰 𝐨𝐭𝐡𝐞𝐫 𝐬𝐢𝐦𝐩𝐥𝐞, 𝐰𝐡𝐨𝐥𝐞𝐬𝐨𝐦𝐞 𝐢𝐧𝐠𝐫𝐞𝐝𝐢𝐞𝐧𝐭𝐬. 𝐅𝐞𝐚𝐭𝐮𝐫𝐢𝐧𝐠 𝐦𝐢𝐥𝐝 𝐠𝐫𝐞𝐞𝐧 𝐜𝐡𝐢𝐥𝐢𝐞𝐬 𝐚𝐧𝐝 𝐚 𝐝𝐚𝐬𝐡 𝐨𝐟 𝐫𝐞𝐝 𝐜𝐡𝐢𝐥𝐢 𝐩𝐨𝐰𝐝𝐞𝐫, 𝐭𝐡𝐢𝐬 𝐯𝐞𝐠𝐚𝐧 𝐫𝐞𝐜𝐢𝐩𝐞 𝐩𝐨𝐩𝐬 𝐰𝐢𝐭𝐡 𝐰𝐨𝐧𝐝𝐞𝐫𝐟𝐮𝐥 𝐭𝐚𝐧𝐠𝐲 𝐚𝐧𝐝 𝐬𝐩𝐢𝐜𝐲 𝐟𝐥𝐚𝐯𝐨𝐫𝐬 𝐢𝐧 𝐞𝐯𝐞𝐫𝐲 𝐛𝐢𝐭𝐞.',
      ImageUrl:'/assets/imagess/CornSalsa.png',
      Price:0


    },
    {
      id :10,
      name :"MoongDal Chat",
      favourite :true,
      star :4.5,
      imageUrl :'/assets/imagess/K.jpeg',
      cooktime : '4-6(min)',
      tags :['Sprouts Recipes'],
      description : '𝐓𝐡𝐢𝐬 𝐌𝐨𝐨𝐧𝐠 𝐃𝐚𝐥 𝐂𝐡𝐚𝐭 𝐢𝐬 𝐚 𝐡𝐞𝐚𝐥𝐭𝐡𝐲 𝐭𝐰𝐢𝐬𝐭 𝐨𝐧 𝐭𝐡𝐞 𝐭𝐫𝐚𝐝𝐢𝐭𝐢𝐨𝐧𝐚𝐥 𝐜𝐡𝐚𝐭 𝐚𝐧𝐝 𝐢𝐬 𝐯𝐞𝐠𝐚𝐧 𝐚𝐧𝐝 𝐚𝐥𝐬𝐨 𝐨𝐢𝐥-𝐟𝐫𝐞𝐞! 𝐈𝐭’𝐬 𝐩𝐨𝐩𝐮𝐥𝐚𝐫 𝐢𝐧 𝐚 𝐜𝐢𝐭𝐲 𝐧𝐚𝐦𝐞𝐝 𝐌𝐨𝐫𝐚𝐝𝐚𝐛𝐚𝐝 𝐢𝐧 𝐔𝐭𝐭𝐚𝐫 𝐏𝐫𝐚𝐝𝐞𝐬𝐡',
      ImageUrl:'/assets/imagess/MoongDalChat.png',
      Price:0

    },
    {
      id :11,
      name :"Fenugreek Brown rice",
      favourite :true,
      star :4.5,
      imageUrl :'/assets/imagess/L.jpeg',
      cooktime : '4-6(min)',
      tags :['Rice'],
      description : '𝐁𝐫𝐨𝐰𝐧 𝐫𝐢𝐜𝐞 𝐢𝐬 𝐧𝐨𝐭 𝐚 𝐛𝐨𝐨𝐧 𝐟𝐨𝐫 𝐣𝐮𝐬𝐭 𝐝𝐢𝐚𝐛𝐞𝐭𝐢𝐜 𝐩𝐞𝐨𝐩𝐥𝐞, 𝐛𝐮𝐭 𝐟𝐨𝐫 𝐚𝐥𝐥 𝐫𝐢𝐜𝐞 𝐞𝐚𝐭𝐞𝐫𝐬. 𝐑𝐢𝐜𝐡 𝐢𝐧 𝐟𝐢𝐛𝐞𝐫, 𝐭𝐡𝐢𝐬 𝐚𝐦𝐚𝐳𝐢𝐧𝐠 𝐫𝐢𝐜𝐞 𝐦𝐚𝐤𝐞𝐬 𝐚 𝐬𝐮𝐩𝐞𝐫 𝐟𝐨𝐨𝐝 𝐢𝐟 𝐲𝐨𝐮 𝐜𝐚𝐧 𝐭𝐨𝐬𝐬 𝐢𝐧 𝐬𝐨𝐦𝐞 𝐧𝐮𝐭𝐫𝐢𝐭𝐢𝐨𝐮𝐬 𝐯𝐞𝐠𝐞𝐭𝐚𝐛𝐥𝐞𝐬 𝐚𝐧𝐝 𝐠𝐫𝐞𝐞𝐧𝐬.',
      ImageUrl:'/assets/imagess/FenugreekMushroomBrownRice.png',
      Price:0

    },
    {
      id :12,
      name :"Whole Wheat Pasta ",
      favourite :true,
      star :4.5,
      imageUrl :'/assets/imagess/M.jpeg',
      cooktime : '4-6(min)',
      tags :['Appetizers'],
      description : '𝐀 𝐭𝐲𝐩𝐞 𝐨𝐟 𝐩𝐚𝐬𝐭𝐚 𝐭𝐡𝐚𝐭 𝐢𝐬 𝐦𝐚𝐝𝐞 𝐰𝐢𝐭𝐡 𝐰𝐡𝐨𝐥𝐞-𝐰𝐡𝐞𝐚𝐭 𝐟𝐥𝐨𝐮𝐫 𝐢𝐧𝐬𝐭𝐞𝐚𝐝 𝐨𝐟 𝐭𝐡𝐞 𝐜𝐨𝐦𝐦𝐨𝐧𝐥𝐲 𝐮𝐬𝐞𝐝 𝐬𝐞𝐦𝐨𝐥𝐢𝐧𝐚, 𝐰𝐡𝐢𝐜𝐡 𝐢𝐬 𝐡𝐚𝐫𝐝-𝐰𝐡𝐞𝐚𝐭 𝐟𝐥𝐨𝐮𝐫. 𝐖𝐡𝐨𝐥𝐞-𝐰𝐡𝐞𝐚𝐭 𝐟𝐥𝐨𝐮𝐫 𝐩𝐫𝐨𝐝𝐮𝐜𝐞𝐬 𝐩𝐚𝐬𝐭𝐚 𝐭𝐡𝐚𝐭 𝐢𝐬 𝐝𝐚𝐫𝐤𝐞𝐫 𝐢𝐧 𝐜𝐨𝐥𝐨𝐫 𝐰𝐢𝐭𝐡 𝐚 𝐡𝐢𝐠𝐡 𝐧𝐮𝐭𝐫𝐢𝐭𝐢𝐨𝐧𝐚𝐥 𝐯𝐚𝐥𝐮𝐞, 𝐜𝐨𝐧𝐭𝐚𝐢𝐧𝐢𝐧𝐠 𝐩𝐫𝐨𝐭𝐞𝐢𝐧 𝐚𝐧𝐝 𝐟𝐢𝐛𝐞𝐫.',
      ImageUrl:'/assets/imagess/WholeWheatPasta.png',
      Price:0
     
      

    },    
    {
      id :13,
      name :"Almond Butter",
      favourite :true,
      star :4.5,
      imageUrl :'/assets/imagess/N.jpeg',
      cooktime : '4-6(min)',
      tags :['Dairy Free Products'],
      description : '𝐀𝐥𝐦𝐨𝐧𝐝 𝐛𝐮𝐭𝐭𝐞𝐫 𝐢𝐬 𝐚 𝐟𝐨𝐨𝐝 𝐩𝐚𝐬𝐭𝐞 𝐦𝐚𝐝𝐞 𝐟𝐫𝐨𝐦 𝐠𝐫𝐢𝐧𝐝𝐢𝐧𝐠 𝐚𝐥𝐦𝐨𝐧𝐝𝐬 𝐢𝐧𝐭𝐨 𝐚 𝐧𝐮𝐭 𝐛𝐮𝐭𝐭𝐞𝐫. 𝐀𝐥𝐦𝐨𝐧𝐝 𝐛𝐮𝐭𝐭𝐞𝐫 𝐦𝐚𝐲 𝐛𝐞 𝐜𝐫𝐮𝐧𝐜𝐡𝐲 𝐨𝐫 𝐬𝐦𝐨𝐨𝐭𝐡, 𝐚𝐧𝐝 𝐢𝐬 𝐠𝐞𝐧𝐞𝐫𝐚𝐥𝐥𝐲 𝐬𝐭𝐢𝐫 𝐨𝐫 𝐧𝐨 𝐬𝐭𝐢𝐫. 𝐀𝐥𝐦𝐨𝐧𝐝 𝐛𝐮𝐭𝐭𝐞𝐫 𝐦𝐚𝐲 𝐛𝐞 𝐞𝐢𝐭𝐡𝐞𝐫 𝐫𝐚𝐰 𝐨𝐫 𝐫𝐨𝐚𝐬𝐭𝐞𝐝, 𝐛𝐮𝐭 𝐭𝐡𝐢𝐬 𝐝𝐞𝐬𝐜𝐫𝐢𝐛𝐞𝐬 𝐭𝐡𝐞 𝐚𝐥𝐦𝐨𝐧𝐝𝐬 𝐭𝐡𝐞𝐦𝐬𝐞𝐥𝐯𝐞𝐬, 𝐩𝐫𝐢𝐨𝐫 𝐭𝐨 𝐠𝐫𝐢𝐧𝐝𝐢𝐧𝐠.',
      ImageUrl:'/assets/imagess/AlmondButter.png',
      Price:0


    }, 
    
    {
      id :14,
      name :"Paneer Capsicum Sabzi",
      favourite :true,
      star :4.5,
      imageUrl :'/assets/imagess/Q.jpeg',
      cooktime : '4-6(min)',
      tags :['Sprouts Recipes'],
      description : '𝐈𝐭 𝐢𝐬 𝐚𝐧 𝐢𝐝𝐞𝐚𝐥 𝐠𝐫𝐚𝐯𝐲 𝐫𝐞𝐜𝐢𝐩𝐞 𝐭𝐨 𝐛𝐞 𝐬𝐞𝐫𝐯𝐞𝐝 𝐰𝐢𝐭𝐡 𝐚 𝐫𝐚𝐧𝐠𝐞 𝐨𝐟 𝐟𝐥𝐚𝐭𝐛𝐫𝐞𝐚𝐝 𝐫𝐞𝐜𝐢𝐩𝐞𝐬 𝐥𝐢𝐤𝐞 𝐫𝐨𝐭𝐢, 𝐜𝐡𝐚𝐩𝐚𝐭𝐡𝐢 𝐚𝐧𝐝 𝐧𝐚𝐚𝐧 𝐛𝐫𝐞𝐚𝐝 𝐨𝐫 𝐚𝐥𝐬𝐨 𝐰𝐢𝐭𝐡 𝐝𝐢𝐟𝐟𝐞𝐫𝐞𝐧𝐭 𝐭𝐲𝐩𝐞𝐬 𝐨𝐟 𝐫𝐢𝐜𝐞. 𝐈𝐭 𝐢𝐬 𝐠𝐞𝐧𝐞𝐫𝐚𝐥𝐥𝐲 𝐦𝐚𝐝𝐞 𝐚𝐬 𝐚 𝐬𝐞𝐦𝐢-𝐝𝐫𝐲 𝐜𝐮𝐫𝐫𝐲 𝐰𝐢𝐭𝐡 𝐦𝐨𝐫𝐞 𝐜𝐡𝐮𝐧𝐤𝐬 𝐨𝐟 𝐜𝐚𝐩𝐬𝐢𝐜𝐮𝐦 𝐚𝐧𝐝 𝐩𝐚𝐧𝐞𝐞𝐫 𝐢𝐧 𝐞𝐚𝐜𝐡 𝐛𝐢𝐭𝐞. ',
      ImageUrl:'/assets/imagess/PaneerCapsicumSabzi.png',
      Price:0
      

    },
    {
      id :15,
      name :"Sprouts Dokla",
      favourite :true,
      star :4.5,
      imageUrl :'/assets/imagess/R.jpeg',
      cooktime : '4-6(min)',
      tags :['Sprouts Recipes'],
      description : '𝐓𝐫𝐚𝐝𝐢𝐭𝐢𝐨𝐧𝐚𝐥𝐥𝐲, 𝐝𝐡𝐨𝐤𝐥𝐚 𝐢𝐬 𝐦𝐚𝐝𝐞 𝐮𝐬𝐢𝐧𝐠 𝐛𝐞𝐬𝐚𝐧 𝐨𝐫 𝐠𝐫𝐚𝐦 𝐟𝐥𝐨𝐮𝐫 𝐚𝐧𝐝 𝐬𝐞𝐦𝐨𝐥𝐢𝐧𝐚, 𝐡𝐨𝐰𝐞𝐯𝐞𝐫, 𝐭𝐡𝐢𝐬 𝐯𝐚𝐫𝐢𝐚𝐭𝐢𝐨𝐧 𝐨𝐟 𝐝𝐡𝐨𝐤𝐥𝐚 𝐢𝐬 𝐜𝐨𝐨𝐤𝐞𝐝 𝐮𝐬𝐢𝐧𝐠 𝐛𝐨𝐢𝐥𝐞𝐝 𝐦𝐨𝐨𝐧𝐠 𝐬𝐩𝐫𝐨𝐮𝐭𝐬, 𝐬𝐩𝐢𝐧𝐚𝐜𝐡 𝐚𝐧𝐝 𝐟𝐫𝐮𝐢𝐭 𝐬𝐚𝐥𝐭 𝐚𝐥𝐨𝐧𝐠 𝐰𝐢𝐭𝐡 𝐠𝐫𝐚𝐭𝐞𝐝 𝐜𝐨𝐜𝐨𝐧𝐮𝐭, 𝐰𝐡𝐢𝐜𝐡 𝐢𝐬 𝐮𝐬𝐞𝐝 𝐚𝐬 𝐭𝐡𝐞 𝐠𝐚𝐫𝐧𝐢𝐬𝐡𝐢𝐧𝐠. 𝐓𝐡𝐢𝐬 𝐢𝐬 𝐚𝐧 𝐞𝐚𝐬𝐲-𝐭𝐨-𝐦𝐚𝐤𝐞 𝐝𝐢𝐬𝐡 𝐭𝐡𝐚𝐭 𝐲𝐨𝐮 𝐜𝐚𝐧 𝐞𝐧𝐣𝐨𝐲 𝐚𝐬 𝐚 𝐬𝐧𝐚𝐜𝐤 𝐚𝐧𝐝 𝐛𝐫𝐞𝐚𝐤𝐟𝐚𝐬𝐭 𝐚𝐬 𝐰𝐞𝐥𝐥, 𝐚𝐧𝐝 𝐰𝐢𝐥𝐥 𝐛𝐞 𝐩𝐫𝐞𝐟𝐞𝐫𝐫𝐞𝐝 𝐛𝐲 𝐭𝐡𝐨𝐬𝐞 𝐰𝐡𝐨 𝐚𝐥𝐰𝐚𝐲𝐬 𝐜𝐨𝐮𝐧𝐭 𝐭𝐡𝐞𝐢𝐫 𝐜𝐚𝐥𝐨𝐫𝐢𝐞𝐬.',
      ImageUrl:'/assets/imagess/SproutsDokla.png',
      Price:0
    },
      {
      id :16,
      name :'Oats Idli',
      favourite :false,
      star :5,
      imageUrl :'/assets/imagess/rava.jpeg',
      cooktime : '20-25(min)',
      tags :['Low Calorie Food'],
      description : '𝐓𝐡𝐞𝐬𝐞 𝐞𝐚𝐬𝐲 𝐨𝐚𝐭𝐬 𝐢𝐝𝐥𝐢 𝐚𝐫𝐞 𝐥𝐢𝐠𝐡𝐭, 𝐟𝐥𝐮𝐟𝐟𝐲, 𝐜𝐚𝐤𝐞𝐬 𝐩𝐞𝐫𝐟𝐞𝐜𝐭 𝐟𝐨𝐫 𝐚 𝐪𝐮𝐢𝐜𝐤 𝐁𝐫𝐞𝐚𝐤𝐟𝐚𝐬𝐭 𝐨𝐫 𝐡𝐞𝐚𝐥𝐭𝐡𝐲 𝐨𝐧-𝐭𝐡𝐞-𝐠𝐨 𝐒𝐧𝐚𝐜𝐤. 𝐌𝐚𝐝𝐞 𝐰𝐢𝐭𝐡 𝐨𝐚𝐭𝐬, 𝐜𝐮𝐫𝐝, 𝐬𝐞𝐦𝐨𝐥𝐢𝐧𝐚, 𝐡𝐞𝐫𝐛𝐬 𝐚𝐧𝐝 𝐬𝐩𝐢𝐜𝐞𝐬, 𝐭𝐡𝐢𝐬 𝐢𝐧𝐬𝐭𝐚𝐧𝐭 𝐨𝐚𝐭𝐬 𝐢𝐝𝐥𝐢 𝐫𝐞𝐜𝐢𝐩𝐞 𝐫𝐞𝐪𝐮𝐢𝐫𝐞𝐬 𝐧𝐨 𝐟𝐞𝐫𝐦𝐞𝐧𝐭𝐚𝐭𝐢𝐨𝐧 𝐚𝐧𝐝 𝐢𝐬 𝐪𝐮𝐢𝐜𝐤 𝐭𝐨 𝐩𝐫𝐞𝐩𝐚𝐫𝐞. 𝐏𝐞𝐫𝐟𝐞𝐜𝐭 𝐟𝐨𝐫 𝐰𝐡𝐞𝐧 𝐲𝐨𝐮’𝐫𝐞 𝐢𝐧 𝐚 𝐭𝐢𝐦𝐞 𝐜𝐫𝐮𝐧𝐜𝐡, 𝐨𝐚𝐭𝐬 𝐢𝐝𝐥𝐢 𝐢𝐬 𝐚𝐧 𝐢𝐧𝐬𝐭𝐚𝐧𝐭 𝐯𝐞𝐫𝐬𝐢𝐨𝐧 𝐮𝐧𝐥𝐢𝐤𝐞 𝐭𝐡𝐞 𝐭𝐫𝐚𝐝𝐢𝐭𝐢𝐨𝐧𝐚𝐥 𝐒𝐨𝐮𝐭𝐡 𝐈𝐧𝐝𝐢𝐚𝐧 𝐈𝐝𝐥𝐢 𝐑𝐞𝐜𝐢𝐩𝐞 𝐭𝐡𝐚𝐭 𝐢𝐬 𝐦𝐚𝐝𝐞 𝐰𝐢𝐭𝐡 𝐭𝐢𝐦𝐞-𝐢𝐧𝐭𝐞𝐧𝐬𝐢𝐯𝐞 𝐦𝐞𝐭𝐡𝐨𝐝 𝐨𝐟 𝐬𝐨𝐚𝐤𝐢𝐧𝐠 𝐫𝐢𝐜𝐞 & 𝐥𝐞𝐧𝐭𝐢𝐥𝐬, 𝐠𝐫𝐢𝐧𝐝𝐢𝐧𝐠 𝐭𝐡𝐞𝐦 𝐚𝐧𝐝 𝐟𝐞𝐫𝐦𝐞𝐧𝐭𝐢𝐧𝐠.',
      ImageUrl:'/assets/imagess/OatsIdli.png',
      Price:0
      },
      {
        id :17,
        name :'Cilantro Lime Rice',
        favourite :false,
        star :5,
        imageUrl :'/assets/imagess/rice.jpeg',
        cooktime : '20-25(min)',
        tags :['Rice'],
        description : '𝐓𝐡𝐢𝐬 𝐡𝐨𝐦𝐞𝐦𝐚𝐝𝐞 𝐥𝐞𝐦𝐨𝐧 𝐜𝐢𝐥𝐚𝐧𝐭𝐫𝐨 𝐫𝐢𝐜𝐞 𝐢𝐬 𝐛𝐞𝐭𝐭𝐞𝐫 𝐭𝐡𝐚𝐧 𝐭𝐡𝐞 𝐨𝐧𝐞 𝐚𝐭 𝐂𝐡𝐢𝐩𝐨𝐭𝐥𝐞 𝐌𝐞𝐱𝐢𝐜𝐚𝐧 𝐆𝐫𝐢𝐥𝐥 𝐨𝐮𝐭𝐥𝐞𝐭𝐬! 𝐈𝐭 𝐢𝐬 𝐩𝐚𝐜𝐤𝐞𝐝 𝐰𝐢𝐭𝐡 𝐟𝐫𝐞𝐬𝐡 𝐡𝐞𝐫𝐛𝐬 𝐚𝐧𝐝 𝐳𝐞𝐬𝐭𝐲 𝐟𝐥𝐚𝐯𝐨𝐫; 𝐧𝐞𝐞𝐝𝐬 𝐣𝐮𝐬𝐭 𝟓 𝐢𝐧𝐠𝐫𝐞𝐝𝐢𝐞𝐧𝐭𝐬, 𝐚𝐧𝐝 𝐜𝐨𝐦𝐞𝐬 𝐭𝐨𝐠𝐞𝐭𝐡𝐞𝐫 𝐢𝐧 𝐮𝐧𝐝𝐞𝐫 𝟐𝟎 𝐦𝐢𝐧𝐮𝐭𝐞𝐬. 𝐀 𝐨𝐧𝐞-𝐩𝐨𝐭 𝐫𝐞𝐜𝐢𝐩𝐞, 𝐭𝐡𝐢𝐬 𝐜𝐚𝐧 𝐛𝐞 𝐬𝐞𝐫𝐯𝐞𝐝 𝐟𝐨𝐫 𝐲𝐨𝐮𝐫 𝐰𝐞𝐞𝐤𝐝𝐚𝐲 𝐦𝐞𝐚𝐥𝐬 𝐨𝐫 𝐚𝐬 𝐚 𝐬𝐢𝐝𝐞 𝐝𝐢𝐬𝐡 𝐰𝐢𝐭𝐡 𝐲𝐨𝐮𝐫 𝐀𝐬𝐢𝐚𝐧 𝐨𝐫 𝐌𝐞𝐱𝐢𝐜𝐚𝐧 𝐰𝐞𝐞𝐤𝐞𝐧𝐝 𝐛𝐫𝐮𝐧𝐜𝐡.',
        ImageUrl:'/assets/imagess/CilantroLimeRice.png',
        Price:0
        },
        {
          id :18,
          name :'Aspagarus Soup',
          favourite :false,
          star :5,
          imageUrl :'/assets/imagess/S.jpeg',
          cooktime : '20-25(min)',
          tags :['Soups'],
          description : '𝐒𝐩𝐫𝐢𝐧𝐠 𝐢𝐬 𝐮𝐩𝐨𝐧 𝐮𝐬, 𝐚𝐧𝐝 𝐚𝐬𝐩𝐚𝐫𝐚𝐠𝐮𝐬 𝐬𝐞𝐚𝐬𝐨𝐧 𝐡𝐚𝐬 𝐚𝐫𝐫𝐢𝐯𝐞𝐝. 𝐈𝐟 𝐲𝐨𝐮 a𝐫𝐞 𝐚 𝐛𝐢𝐠 𝐟𝐚𝐧 𝐨𝐟 𝐭𝐡𝐢𝐬 𝐯𝐞𝐠𝐞𝐭𝐚𝐛𝐥𝐞, 𝐧𝐨 𝐝𝐨𝐮𝐛𝐭 𝐲𝐨𝐮 𝐚𝐥𝐫𝐞𝐚𝐝𝐲 𝐥𝐨𝐯𝐞 𝐭𝐨 𝐫𝐨𝐚𝐬𝐭 𝐢𝐭 𝐮𝐧𝐭𝐢𝐥 𝐭𝐡𝐞 𝐭𝐢𝐩𝐬 𝐚𝐫𝐞 𝐜𝐫𝐮𝐧𝐜𝐡𝐲 𝐟𝐨𝐫 𝐭𝐡𝐞 𝐮𝐥𝐭𝐢𝐦𝐚𝐭𝐞 𝐟𝐥𝐚𝐯𝐨𝐫 𝐩𝐚𝐲𝐨𝐟𝐟.𝐅𝐨𝐫 𝐚𝐧𝐨𝐭𝐡𝐞𝐫 𝐰𝐚𝐲 𝐭𝐨 𝐜𝐨𝐨𝐤 𝐚𝐬𝐩𝐚𝐫𝐚𝐠𝐮𝐬, 𝐠𝐢𝐯𝐞 𝐭𝐡𝐢𝐬 𝐜𝐫𝐞𝐚𝐦𝐲 𝐬𝐨𝐮𝐩 𝐚 𝐭𝐚𝐬𝐭𝐞: 𝐖𝐞 𝐦𝐚𝐫𝐫𝐲 𝐢𝐭 𝐰𝐢𝐭𝐡 𝐚 𝐭𝐨𝐮𝐜𝐡 𝐨𝐟 𝐛𝐮𝐭𝐭𝐞𝐫 𝐚𝐧𝐝 𝐬𝐨𝐦𝐞 𝐠𝐚𝐫𝐥𝐢𝐜 𝐭𝐨 𝐬𝐨𝐟𝐭𝐞𝐧 𝐭𝐡𝐞 𝐠𝐫𝐚𝐬𝐬𝐲 𝐧𝐨𝐭𝐞𝐬, 𝐬𝐢𝐦𝐦𝐞𝐫 𝐢𝐭 𝐰𝐢𝐭𝐡 𝐛𝐫𝐨𝐭𝐡 𝐮𝐧𝐭𝐢𝐥 𝐭𝐞𝐧𝐝𝐞𝐫, 𝐭𝐡𝐞𝐧 𝐛𝐥𝐞𝐧𝐝 𝐢𝐭 𝐮𝐧𝐭𝐢𝐥 𝐬𝐦𝐨𝐨𝐭𝐡 𝐰𝐢𝐭𝐡 𝐚 𝐡𝐞𝐚𝐥𝐭𝐡𝐲 𝐝𝐨𝐬𝐞 𝐨𝐟 𝐜𝐫𝐞𝐚𝐦.',
          ImageUrl:'/assets/imagess/AspagarusSoup.png',
          Price:0     
        },
          {
            id :19,
            name :' Almond Milk',
            favourite :false,
            star :5,
            imageUrl :'/assets/imagess/T.jpeg',
            cooktime : '20-25(min)',
            tags :['Fruit Juices'],
            description : '𝐀𝐥𝐦𝐨𝐧𝐝 𝐦𝐢𝐥𝐤 𝐢𝐬 𝐚 𝐩𝐥𝐚𝐧𝐭-𝐛𝐚𝐬𝐞𝐝 𝐛𝐞𝐯𝐞𝐫𝐚𝐠𝐞 𝐦𝐚𝐝𝐞 𝐟𝐫𝐨𝐦 𝐟𝐢𝐥𝐭𝐞𝐫𝐞𝐝 𝐚𝐥𝐦𝐨𝐧𝐝𝐬 𝐚𝐧𝐝 𝐰𝐚𝐭𝐞𝐫. 𝐈𝐭 𝐢𝐬 𝐧𝐚𝐭𝐮𝐫𝐚𝐥𝐥𝐲 𝐝𝐚𝐢𝐫𝐲- 𝐚𝐧𝐝 𝐥𝐚𝐜𝐭𝐨𝐬𝐞-𝐟𝐫𝐞𝐞, 𝐦𝐚𝐤𝐢𝐧𝐠 𝐢𝐭 𝐚 𝐠𝐨𝐨𝐝 𝐨𝐩𝐭𝐢𝐨𝐧 𝐟𝐨𝐫 𝐭𝐡𝐨𝐬𝐞 𝐚𝐯𝐨𝐢𝐝𝐢𝐧𝐠 𝐝𝐚𝐢𝐫𝐲.',
            ImageUrl:'/assets/imagess/AlmondMilk (1).png',
            Price:0
            },
            {
              id :20,
              name :'Dry Fruit Milk shake',
              favourite :false,
              star :5,
              imageUrl :'/assets/imagess/U.jpeg',
              cooktime : '20-25(min)',
              tags :['Fruit Juices'],
              description : '𝐃𝐫𝐲 𝐟𝐫𝐮𝐢𝐭𝐬 𝐦𝐢𝐥𝐤𝐬𝐡𝐚𝐤𝐞 𝐢𝐬 𝐯𝐞𝐫𝐲 𝐩𝐨𝐩𝐮𝐥𝐚𝐫 𝐢𝐧 𝐈𝐧𝐝𝐢𝐚𝐧 𝐫𝐞𝐬𝐭𝐚𝐮𝐫𝐚𝐧𝐭𝐬 𝐚𝐧𝐝 𝐢𝐧 𝐭𝐡𝐞 𝐣𝐮𝐢𝐜𝐞 𝐜𝐞𝐧𝐭𝐞𝐫𝐬 𝐟𝐨𝐮𝐧𝐝 𝐢𝐧 𝐭𝐡𝐞 𝐜𝐢𝐭𝐢𝐞𝐬 𝐨𝐟 𝐌𝐮𝐦𝐛𝐚𝐢 𝐚𝐧𝐝 𝐃𝐞𝐥𝐡𝐢. 𝐈𝐭 𝐢𝐬 𝐝𝐞𝐥𝐢𝐜𝐢𝐨𝐮𝐬, 𝐩𝐚𝐜𝐤𝐞𝐝 𝐰𝐢𝐭𝐡 𝐧𝐮𝐭𝐫𝐢𝐞𝐧𝐭𝐬 𝐥𝐢𝐤𝐞 𝐩𝐫𝐨𝐭𝐞𝐢𝐧, 𝐢𝐫𝐨𝐧, 𝐜𝐚𝐥𝐜𝐢𝐮𝐦, 𝐚𝐧𝐝 𝐢𝐬 𝐠𝐫𝐞𝐚𝐭 𝐟𝐨𝐫 𝐛𝐨𝐨𝐬𝐭𝐢𝐧𝐠 𝐞𝐧𝐞𝐫𝐠𝐲 𝐥𝐞𝐯𝐞𝐥𝐬. 𝐘𝐨𝐮 𝐜𝐚𝐧 𝐚𝐥𝐬𝐨 𝐝𝐫𝐢𝐧𝐤 𝐭𝐡𝐢𝐬 𝐦𝐢𝐥𝐤𝐬𝐡𝐚𝐤𝐞 𝐝𝐮𝐫𝐢𝐧𝐠 𝐟𝐚𝐬𝐭𝐢𝐧𝐠 𝐬𝐞𝐚𝐬𝐨𝐧𝐬. 𝐓𝐡𝐢𝐬 𝐦𝐢𝐥𝐤𝐬𝐡𝐚𝐤𝐞 𝐢𝐬 𝐯𝐞𝐫𝐲 𝐟𝐢𝐥𝐥𝐢𝐧𝐠 𝐬𝐨 𝐈 𝐮𝐬𝐮𝐚𝐥𝐥𝐲 𝐬𝐤𝐢𝐩 𝐚 𝐦𝐞𝐚𝐥 𝐚𝐟𝐭𝐞𝐫 𝐝𝐫𝐢𝐧𝐤𝐢𝐧𝐠 𝐢𝐭.',
              ImageUrl:'/assets/imagess/DryFruitMilkshake.png',
              Price:0
              },
              {
                id :21,
                name :'Ginger beetroot juice',
                favourite :false,
                star :5,
                imageUrl :'/assets/imagess/V.jpeg',
                cooktime : '20-25(min)',
                tags :['Fruit Juices'],
                description : '𝐓𝐡𝐢𝐬 𝐛𝐞𝐞𝐭𝐫𝐨𝐨𝐭, 𝐠𝐢𝐧𝐠𝐞𝐫, 𝐚𝐧𝐝 𝐥𝐞𝐦𝐨𝐧 𝐣𝐮𝐢𝐜𝐞 𝐢𝐬 𝐚 𝐠𝐫𝐞𝐚𝐭 𝐝𝐞𝐭𝐨𝐱 𝐣𝐮𝐢𝐜𝐞 𝐨𝐩𝐭𝐢𝐨𝐧 𝐟𝐨𝐫 𝐚𝐧𝐲𝐨𝐧𝐞 𝐟𝐨𝐥𝐥𝐨𝐰𝐢𝐧𝐠 𝐚 𝐣𝐮𝐢𝐜𝐞 𝐜𝐥𝐞𝐚𝐧𝐬𝐞, 𝐥𝐨𝐨𝐤𝐢𝐧𝐠 𝐟𝐨𝐫 𝐚𝐧 𝐢𝐦𝐦𝐮𝐧𝐞 𝐛𝐨𝐨𝐬𝐭𝐞𝐫, 𝐚𝐧𝐝 𝐰𝐚𝐧𝐭𝐢𝐧𝐠 𝐭𝐨 𝐩𝐚𝐜𝐤 𝐢𝐧 𝐦𝐨𝐫𝐞 𝐞𝐬𝐬𝐞𝐧𝐭𝐢𝐚𝐥 𝐧𝐮𝐭𝐫𝐢𝐞𝐧𝐭𝐬 𝐟𝐨𝐫 𝐭𝐡𝐞 𝐛𝐨𝐝𝐲.𝐏𝐥𝐮𝐬, 𝐭𝐡𝐞 𝐜𝐨𝐦𝐛𝐢𝐧𝐚𝐭𝐢𝐨𝐧 𝐨𝐟 𝐛𝐞𝐞𝐭𝐬 𝐚𝐧𝐝 𝐠𝐢𝐧𝐠𝐞𝐫 𝐡𝐚𝐬 𝐚 𝐰𝐡𝐨𝐥𝐞 𝐯𝐚𝐫𝐢𝐞𝐭𝐲 𝐨𝐟 𝐡𝐞𝐚𝐥𝐭𝐡 𝐛𝐞𝐧𝐞𝐟𝐢𝐭𝐬.',
                ImageUrl:'/assets/imagess/GingerBeetrootJuice.png',
                Price:0
                },
                {
                  id :22,
                  name :'Broccoli Salad',
                  favourite :false,
                  star :5,
                  imageUrl :'/assets/imagess/veggie.jpeg',
                  cooktime : '20-25(min)',
                  tags :['Salads'],
                  description : '𝐓𝐡𝐢𝐬 𝐢𝐬 𝐚 𝐡𝐞𝐚𝐥𝐭𝐡𝐢𝐞𝐫 𝐬𝐩𝐢𝐧 𝐨𝐧 𝐭𝐡𝐞 𝐭𝐫𝐚𝐝𝐢𝐭𝐢𝐨𝐧𝐚𝐥 𝐦𝐢𝐝 𝐰𝐞𝐬𝐭 𝐛𝐫𝐨𝐜𝐜𝐨𝐥𝐢 𝐬𝐚𝐥𝐚𝐝 𝐛𝐮𝐭 𝐢𝐭’𝐬 𝐞𝐯𝐞𝐫𝐲 𝐛𝐢𝐭 𝐚𝐬 𝐟𝐥𝐚𝐯𝐨𝐫𝐟𝐮𝐥. 𝐒𝐦𝐚𝐥𝐥 𝐛𝐫𝐨𝐜𝐜𝐨𝐥𝐢 𝐟𝐥𝐨𝐫𝐞𝐭𝐬 𝐚𝐫𝐞 𝐦𝐚𝐫𝐢𝐧𝐚𝐭𝐞𝐝 𝐢𝐧 𝐚 𝐜𝐫𝐞𝐚𝐦𝐲/𝐭𝐚𝐧𝐠𝐲 𝐝𝐫𝐞𝐬𝐬𝐢𝐧𝐠 𝐚𝐥𝐨𝐧𝐠 𝐰𝐢𝐭𝐡 𝐝𝐫𝐢𝐞𝐝 𝐜𝐫𝐚𝐧𝐛𝐞𝐫𝐫𝐢𝐞𝐬 𝐚𝐧𝐝 𝐬𝐦𝐨𝐤𝐲 𝐭𝐚𝐦𝐚𝐫𝐢-𝐫𝐨𝐚𝐬𝐭𝐞𝐝 𝐚𝐥𝐦𝐨𝐧𝐝𝐬. 𝐈𝐭’𝐬 𝐚 𝐨𝐧𝐞-𝐛𝐨𝐰𝐥 𝐫𝐞𝐜𝐢𝐩𝐞 𝐭𝐡𝐚𝐭’𝐬 𝐠𝐫𝐞𝐚𝐭 𝐚𝐬 𝐚 𝐩𝐚𝐜𝐤-𝐚𝐛𝐥𝐞 𝐩𝐢𝐜𝐧𝐢𝐜 𝐬𝐚𝐥𝐚𝐝 𝐨𝐫 𝐚𝐧 𝐞𝐚𝐬𝐲 𝐦𝐚𝐤𝐞-𝐚𝐡𝐞𝐚𝐝 𝐥𝐮𝐧𝐜𝐡.',
                  ImageUrl:'/assets/imagess/BroccoliSalad.png',
                  Price:0
                  },
                  {
                    id :23,
                    name :'Peanut Truffles',
                    favourite :false,
                    star :5,
                    imageUrl :'/assets/imagess/W.jpeg',
                    cooktime : '20-25(min)',
                    tags :['Dairy Free Products'],
                    description : '𝐓𝐡𝐞𝐬𝐞 𝟒-𝐢𝐧𝐠𝐫𝐞𝐝𝐢𝐞𝐧𝐭 𝐏𝐞𝐚𝐧𝐮𝐭 𝐁𝐮𝐭𝐭𝐞𝐫 𝐓𝐫𝐮𝐟𝐟𝐥𝐞𝐬 𝐚𝐫𝐞 𝐜𝐫𝐞𝐚𝐦𝐲, 𝐧𝐨-𝐛𝐚𝐤𝐞 𝐩𝐞𝐚𝐧𝐮𝐭 𝐛𝐮𝐭𝐭𝐞𝐫 𝐛𝐚𝐥𝐥𝐬 𝐝𝐢𝐩𝐩𝐞𝐝 𝐢𝐧 𝐰𝐡𝐢𝐭𝐞 𝐜𝐡𝐨𝐜𝐨𝐥𝐚𝐭𝐞 𝐚𝐧𝐝 𝐝𝐞𝐜𝐨𝐫𝐚𝐭𝐞𝐝 𝐟𝐨𝐫 𝐚𝐧𝐲 𝐨𝐜𝐜𝐚𝐬𝐢𝐨𝐧. 𝐓𝐡𝐞𝐲 𝐚𝐫𝐞 𝐞𝐚𝐬𝐲 𝐭𝐨 𝐦𝐚𝐤𝐞 𝐚𝐧𝐝 𝐛𝐞𝐭𝐭𝐞𝐫 𝐭𝐡𝐚𝐧 𝐚𝐧𝐲 𝐩𝐞𝐚𝐧𝐮𝐭 𝐛𝐮𝐭𝐭𝐞𝐫 𝐜𝐮𝐩 𝐜𝐚𝐧𝐝𝐲! 𝐃𝐢𝐩 𝐭𝐡𝐞𝐦 𝐢𝐧 𝐰𝐡𝐢𝐭𝐞 𝐜𝐡𝐨𝐜𝐨𝐥𝐚𝐭𝐞, 𝐦𝐢𝐥𝐤 𝐜𝐡𝐨𝐜𝐨𝐥𝐚𝐭𝐞 𝐨𝐫 𝐝𝐚𝐫𝐤 𝐜𝐡𝐨𝐜𝐨𝐥𝐚𝐭𝐞 (𝐨𝐫 𝐚𝐥𝐥 𝟑!) 𝐓𝐡𝐞𝐧 𝐝𝐞𝐜𝐨𝐫𝐚𝐭𝐞 𝐭𝐡𝐞𝐦 𝐟𝐨𝐫 𝐚𝐧𝐲 𝐨𝐜𝐜𝐚𝐬𝐢𝐨𝐧! 𝐓𝐡𝐞𝐲 𝐚𝐫𝐞 𝐩𝐞𝐫𝐟𝐞𝐜𝐭 𝐟𝐨𝐫 𝐚 𝐡𝐨𝐥𝐢𝐝𝐚𝐲 𝐜𝐨𝐨𝐤𝐢𝐞 𝐞𝐱𝐜𝐡𝐚𝐧𝐠𝐞 𝐨𝐫 𝐬𝐰𝐚𝐩!',
                    ImageUrl:'/assets/imagess/PeanutButterTruffles.png',
                    Price:0
                    },
                    {
                      id :24,
                      name :'Japanese Salmon Avocado Rice',
                      favourite :false,
                      star :5,
                      imageUrl :'/assets/imagess/X.jpeg',
                      cooktime : '20-25(min)',
                      tags :['Rice'],
                      description : '𝐇𝐚𝐯𝐢𝐧𝐠 𝐚 𝐛𝐚𝐭𝐜𝐡 𝐨𝐟 𝐭𝐞𝐫𝐢𝐲𝐚𝐤𝐢 𝐬𝐚𝐮𝐜𝐞 𝐢𝐧 𝐲𝐨𝐮𝐫 𝐟𝐫𝐢𝐝𝐠𝐞 𝐚𝐭 𝐚𝐥𝐥 𝐭𝐢𝐦𝐞𝐬 (𝐢𝐭 𝐥𝐚𝐬𝐭𝐬 𝐟𝐨𝐫𝐞𝐯𝐞𝐫) 𝐚𝐥𝐥𝐨𝐰𝐬 𝐲𝐨𝐮 𝐭𝐨 𝐩𝐮𝐥𝐥 𝐭𝐨𝐠𝐞𝐭𝐡𝐞𝐫 𝐚 𝐝𝐢𝐧𝐧𝐞𝐫 𝐥𝐢𝐤𝐞 𝐭𝐡𝐢𝐬 𝐫𝐢𝐜𝐞 𝐛𝐨𝐰𝐥 𝐰𝐢𝐭𝐡 𝐭𝐞𝐫𝐢𝐲𝐚𝐤𝐢-𝐠𝐥𝐚𝐳𝐞𝐝 𝐬𝐚𝐥𝐦𝐨𝐧 𝐚𝐧𝐝 𝐚𝐯𝐨𝐜𝐚𝐝𝐨 𝐢𝐧 𝐭𝐡𝐞 𝐚𝐦𝐨𝐮𝐧𝐭 𝐨𝐟 𝐭𝐢𝐦𝐞 𝐢𝐭 𝐭𝐚𝐤𝐞𝐬 𝐭𝐨 𝐬𝐭𝐞𝐚𝐦 𝐚 𝐩𝐨𝐭 𝐨𝐟 𝐫𝐢𝐜𝐞. 𝐎𝐧𝐜𝐞 𝐲𝐨𝐮 𝐡𝐚𝐯𝐞 𝐭𝐡𝐞 𝐫𝐢𝐜𝐞 𝐨𝐧, 𝐚𝐥𝐥 𝐲𝐨𝐮 𝐠𝐨𝐭 𝐭𝐨 𝐝𝐨 𝐢𝐬 𝐬𝐞𝐚𝐫 𝐭𝐡𝐞 𝐬𝐚𝐥𝐦𝐨𝐧, 𝐝𝐢𝐜𝐞 𝐮𝐩 𝐬𝐨𝐦𝐞 𝐚𝐯𝐨𝐜𝐚𝐝𝐨 𝐚𝐧𝐝 𝐜𝐮𝐜𝐮𝐦𝐛𝐞𝐫, 𝐬𝐥𝐢𝐜𝐞 𝐚 𝐜𝐨𝐮𝐩𝐥𝐞 𝐨𝐟 𝐬𝐜𝐚𝐥𝐥𝐢𝐨𝐧𝐬, 𝐚𝐧𝐝 𝐩𝐮𝐥𝐥 𝐭𝐡𝐚𝐭 𝐥𝐞𝐟𝐭𝐨𝐯𝐞𝐫 𝐭𝐞𝐫𝐢𝐲𝐚𝐤𝐢 𝐬𝐚𝐮𝐜𝐞 𝐨𝐮𝐭 𝐨𝐟 𝐭𝐡𝐞 𝐟𝐫𝐢𝐝𝐠𝐞.',
                      ImageUrl:'/assets/imagess/JapaneseSalmonAndAvocadoRice.png',
                      Price:0
                      },
                      {
                        id :25,
                        name :'Chocolate Muffins',
                        favourite :false,
                        star :5,
                        imageUrl :'/assets/imagess/Y.jpeg',
                        cooktime : '20-25(min)',
                        tags :['Desserts'],
                        description : '𝐁𝐚𝐤𝐞𝐫𝐲-𝐬𝐭𝐲𝐥𝐞 𝐜𝐡𝐨𝐜𝐨𝐥𝐚𝐭𝐞 𝐜𝐡𝐢𝐩 𝐦𝐮𝐟𝐟𝐢𝐧𝐬 𝐚𝐫𝐞 𝐁𝐈𝐆 𝐢𝐧 𝐬𝐢𝐳𝐞, 𝐟𝐥𝐚𝐯𝐨𝐫, 𝐚𝐧𝐝 𝐭𝐞𝐱𝐭𝐮𝐫𝐞. 𝐓𝐡𝐞𝐲 𝐚𝐫𝐞 𝐝𝐞𝐥𝐢𝐜𝐢𝐨𝐮𝐬𝐥𝐲 𝐬𝐨𝐟𝐭 𝐚𝐧𝐝 𝐦𝐨𝐢𝐬𝐭 𝐰𝐢𝐭𝐡 𝐚 𝐜𝐫𝐚𝐜𝐤𝐥𝐲 𝐦𝐮𝐟𝐟𝐢𝐧 𝐭𝐨𝐩. 𝐂𝐢𝐧𝐧𝐚𝐦𝐨𝐧 𝐬𝐩𝐢𝐜𝐞𝐝 𝐚𝐧𝐝 𝐥𝐨𝐚𝐝𝐞𝐝 𝐰𝐢𝐭𝐡 𝐜𝐡𝐨𝐜𝐨𝐥𝐚𝐭𝐞 𝐜𝐡𝐢𝐩𝐬, 𝐲𝐨𝐮 𝐰𝐢𝐥𝐥 𝐧𝐞𝐯𝐞𝐫 𝐭𝐮𝐫𝐧 𝐭𝐨 𝐚𝐧𝐨𝐭𝐡𝐞𝐫 𝐦𝐮𝐟𝐟𝐢𝐧 𝐫𝐞𝐜𝐢𝐩𝐞 𝐚𝐠𝐚𝐢𝐧.',
                        ImageUrl:'/assets/imagess/ChocolateChipMuffins.png',
                        Price:0
                        },
                        {
                          id :26,
                          name :'Oreo Sundae',
                          favourite :false,
                          star :5,
                          imageUrl :'/assets/imagess/Z.jpeg',
                          cooktime : '20-25(min)',
                          tags :['Desserts'],
                          description : '𝐀𝐧 𝐞𝐚𝐬𝐲 𝐚𝐧𝐝 𝐬𝐢𝐦𝐩𝐥𝐞 𝐫𝐞𝐟𝐫𝐞𝐬𝐡𝐢𝐧𝐠 𝐢𝐜𝐞 𝐜𝐫𝐞𝐚𝐦 𝐫𝐞𝐜𝐢𝐩𝐞 𝐦𝐚𝐝𝐞 𝐰𝐢𝐭𝐡 𝐣𝐮𝐬𝐭 𝟑 𝐢𝐧𝐠𝐫𝐞𝐝𝐢𝐞𝐧𝐭𝐬 – 𝐛𝐢𝐬𝐜𝐮𝐢𝐭𝐬, 𝐜𝐫𝐞𝐚𝐦 𝐚𝐧𝐝 𝐜𝐨𝐧𝐝𝐞𝐧𝐬𝐞𝐝 𝐦𝐢𝐥𝐤. 𝐭𝐞𝐜𝐡𝐧𝐢𝐜𝐚𝐥𝐥𝐲 𝐭𝐡𝐞𝐫𝐞 𝐢𝐬 𝐧𝐨 𝐚𝐝𝐝𝐞𝐝 𝐬𝐮𝐠𝐚𝐫 𝐛𝐮𝐭 𝐭𝐡𝐞 𝐜𝐨𝐧𝐝𝐞𝐧𝐬𝐞𝐝 𝐦𝐢𝐥𝐤 𝐝𝐨𝐞𝐬 𝐜𝐨𝐧𝐭𝐚𝐢𝐧 𝐬𝐮𝐠𝐚𝐫 𝐚𝐧𝐝 𝐦𝐨𝐫𝐞 𝐭𝐡𝐚𝐧 𝐬𝐮𝐟𝐟𝐢𝐜𝐢𝐞𝐧𝐭 𝐟𝐨𝐫 𝐭𝐡𝐢𝐬 𝐫𝐞𝐜𝐢𝐩𝐞. 𝐛𝐚𝐬𝐢𝐜𝐚𝐥𝐥𝐲, 𝐢𝐭 𝐢𝐬 𝐚𝐧 𝐞𝐱𝐭𝐞𝐧𝐬𝐢𝐨𝐧 𝐭𝐨 𝐭𝐡𝐞 𝐟𝐥𝐚𝐯𝐨𝐫𝐞𝐝 𝐯𝐚𝐧𝐢𝐥𝐥𝐚 𝐢𝐜𝐞 𝐜𝐫𝐞𝐚𝐦 𝐰𝐢𝐭𝐡 𝐭𝐨𝐩𝐩𝐢𝐧𝐠𝐬 𝐨𝐟 𝐜𝐫𝐮𝐬𝐡𝐞𝐝 𝐨𝐫𝐞𝐨 𝐛𝐢𝐬𝐜𝐮𝐢𝐭𝐬 𝐟𝐨𝐫 𝐭𝐡𝐚𝐭 𝐜𝐫𝐮𝐧𝐜𝐡𝐲 𝐚𝐧𝐝 𝐛𝐢𝐬𝐜𝐮𝐢𝐭 𝐟𝐥𝐚𝐯𝐨𝐫𝐞𝐝 𝐢𝐜𝐞 𝐜𝐫𝐞𝐚𝐦.',
                          ImageUrl:'/assets/imagess/OreoSundae.png',
                          Price:0
                          },
                          {
                            id :27,
                            name :'Cucumber Capsicum Celery Salad',
                            favourite :false,
                            star :5,
                            imageUrl :'/assets/imagess/Cucumber Capsicum Celery Salad (1).jpeg',
                            cooktime : '20-25(min)',
                            tags :['Salads'],
                            description : '𝐔𝐬𝐞 𝐮𝐩 𝐭𝐡𝐞 𝐛𝐨𝐮𝐧𝐭𝐲 𝐨𝐟 𝐬𝐮𝐦𝐦𝐞𝐫 𝐰𝐢𝐭𝐡 𝐭𝐡𝐢𝐬 𝐫𝐞𝐟𝐫𝐞𝐬𝐡𝐢𝐧𝐠 𝐚𝐧𝐝 𝐜𝐫𝐢𝐬𝐩𝐲 𝐬𝐚𝐥𝐚𝐝 𝐰𝐢𝐭𝐡 𝐜𝐮𝐜𝐮𝐦𝐛𝐞𝐫𝐬, 𝐫𝐢𝐜𝐞 𝐯𝐢𝐧𝐞𝐠𝐚𝐫, 𝐬𝐚𝐥𝐭, 𝐩𝐞𝐩𝐩𝐞𝐫, 𝐚𝐧𝐝 𝐜𝐡𝐨𝐩𝐩𝐞𝐝 𝐟𝐫𝐞𝐬𝐡 𝐝𝐢𝐥𝐥 𝐨𝐫 𝐛𝐚𝐬𝐢𝐥. 𝐓𝐡𝐢𝐬 𝐞𝐚𝐬𝐲 𝐜𝐮𝐜𝐮𝐦𝐛𝐞𝐫 𝐬𝐚𝐥𝐚𝐝 𝐜𝐨𝐧𝐬𝐢𝐬𝐭𝐬 𝐨𝐟 𝐧𝐨𝐭𝐡𝐢𝐧𝐠 𝐦𝐨𝐫𝐞 𝐭𝐡𝐚𝐧 𝐭𝐡𝐞 𝐜𝐮𝐜𝐮𝐦𝐛𝐞𝐫𝐬, 𝐬𝐨𝐦𝐞 𝐫𝐢𝐜𝐞 𝐯𝐢𝐧𝐞𝐠𝐚𝐫, 𝐞𝐢𝐭𝐡𝐞𝐫 𝐝𝐢𝐥𝐥 𝐨𝐫 𝐛𝐚𝐬𝐢𝐥, 𝐚𝐧𝐝 𝐬𝐚𝐥𝐭 𝐚𝐧𝐝 𝐩𝐞𝐩𝐩𝐞𝐫.',
                            ImageUrl:'/assets/imagess/CucumberCapsicumCelerySalad.png',
                            Price:0
                            },
                            {
                              id :28,
                              name :'Date Walnut Balls',
                              favourite :false,
                              star :5,
                              imageUrl :'/assets/imagess/Date and Walnut Balls.jpg',
                              cooktime : '20-25(min)',
                              tags :['Desserts'],
                              description : '𝐃𝐚𝐭𝐞 𝐞𝐧𝐞𝐫𝐠𝐲 𝐛𝐚𝐥𝐥𝐬 𝐚𝐫𝐞 𝐭𝐡𝐞 𝐩𝐞𝐫𝐟𝐞𝐜𝐭 𝐡𝐞𝐚𝐥𝐭𝐡𝐲 𝐬𝐧𝐚𝐜𝐤. 𝐅𝐢𝐧𝐞𝐥𝐲 𝐠𝐫𝐨𝐮𝐧𝐝 𝐝𝐚𝐭𝐞𝐬 𝐚𝐧𝐝 𝐰𝐚𝐥𝐧𝐮𝐭𝐬 𝐚𝐫𝐞 𝐬𝐡𝐚𝐩𝐞𝐝 𝐢𝐧𝐭𝐨 𝐥𝐢𝐭𝐭𝐥𝐞 𝐛𝐢𝐭𝐞𝐬, 𝐚𝐧𝐝 𝐞𝐧𝐣𝐨𝐲𝐞𝐝 𝐚𝐬 𝐚𝐧 𝐞𝐚𝐬𝐲 𝐭𝐫𝐞𝐚𝐭.',
                              ImageUrl:'/assets/imagess/DateAndWalnutBalls.png',
                              Price:0
                              },
                              {
                                id :29,
                                name :'Honey Lemon Water',
                                favourite :false,
                                star :5,
                                imageUrl :'/assets/imagess/Honey Lemon Water.jpg',
                                cooktime : '20-25(min)',
                                tags :['Low Calorie Food'],
                                description : '𝐒𝐢𝐩𝐩𝐢𝐧𝐠 𝐨𝐧 𝐚 𝐡𝐨𝐭 𝐜𝐮𝐩 𝐨𝐟 𝐡𝐨𝐧𝐞𝐲 𝐥𝐞𝐦𝐨𝐧 𝐰𝐚𝐭𝐞𝐫 𝐢𝐬 𝐛𝐨𝐭𝐡 𝐭𝐚𝐬𝐭𝐲 𝐚𝐧𝐝 𝐬𝐨𝐨𝐭𝐡𝐢𝐧𝐠. 𝐈𝐭 𝐡𝐚𝐬 𝐚𝐥𝐬𝐨 𝐛𝐞𝐞𝐧 𝐩𝐫𝐨𝐦𝐨𝐭𝐞𝐝 𝐚𝐬 𝐚 𝐡𝐞𝐚𝐥𝐢𝐧𝐠 𝐞𝐥𝐢𝐱𝐢𝐫 𝐢𝐧 𝐭𝐡𝐞 𝐡𝐞𝐚𝐥𝐭𝐡 𝐚𝐧𝐝 𝐰𝐞𝐥𝐥𝐧𝐞𝐬𝐬 𝐰𝐨𝐫𝐥𝐝. 𝐓𝐡𝐞𝐫𝐞 𝐚𝐫𝐞 𝐜𝐥𝐚𝐢𝐦𝐬 𝐭𝐡𝐚𝐭 𝐭𝐡𝐢𝐬 𝐝𝐫𝐢𝐧𝐤 𝐜𝐚𝐧 𝐡𝐞𝐥𝐩 𝐦𝐞𝐥𝐭 𝐟𝐚𝐭, 𝐜𝐥𝐞𝐚𝐫 𝐮𝐩 𝐚𝐜𝐧𝐞 𝐚𝐧𝐝 “𝐟𝐥𝐮𝐬𝐡 𝐨𝐮𝐭” 𝐭𝐨𝐱𝐢𝐧𝐬 𝐟𝐫𝐨𝐦 𝐭𝐡𝐞 𝐛𝐨𝐝𝐲. ',
                                ImageUrl:'/assets/imagess/HoneyLemonWater.png',
                                Price:0
                                },
                                {
                                  id :30,
                                  name :'Yule Slice',
                                  favourite :false,
                                  star :5,
                                  imageUrl :'/assets/imagess/Yule Slice.jpeg',
                                  cooktime : '20-25(min)',
                                  tags :['Dairy Free Products'],
                                  description : '𝐘𝐮𝐥𝐞 𝐬𝐥𝐢𝐜𝐞 𝐢𝐬 𝐚 𝐃𝐚𝐢𝐫𝐲-𝐟𝐫𝐞𝐞 𝐫𝐞𝐜𝐢𝐩𝐞 𝐟𝐨𝐫 𝟒 - 𝟖 𝐩𝐞𝐨𝐩𝐥𝐞, 𝐭𝐚𝐤𝐞𝐬 𝐨𝐧𝐥𝐲 𝟒𝟓 𝐦𝐢𝐧𝐬; 𝐫𝐞𝐜𝐢𝐩𝐞 𝐡𝐚𝐬 𝐰𝐚𝐥𝐧𝐮𝐭, 𝐡𝐚𝐳𝐞𝐥𝐧𝐮𝐭 𝐚𝐧𝐝 𝐛𝐥𝐚𝐧𝐜𝐡𝐞𝐝 𝐚𝐥𝐦𝐨𝐧𝐝.',
                                  ImageUrl:'/assets/imagess/YuleSlice.png',
                                  Price:0
                                  },
                                  {
                                    id :31,
                                    name :'Matki Sabzi',
                                    favourite :false,
                                    star :5,
                                    imageUrl :'/assets/imagess/Matki Sabzi (1).jpg',
                                    cooktime : '20-25(min)',
                                    tags :['Sprouts Recipes'],
                                    description : '𝐈𝐭 𝐡𝐚𝐬 𝐡𝐢𝐠𝐡 𝐚𝐧𝐭𝐢𝐨𝐱𝐢𝐝𝐚𝐧𝐭 𝐥𝐞𝐯𝐞𝐥𝐬 𝐰𝐡𝐢𝐜𝐡 𝐫𝐞𝐝𝐮𝐜𝐞 𝐭𝐡𝐞 𝐫𝐢𝐬𝐤 𝐨𝐟 𝐜𝐚𝐭𝐜𝐡𝐢𝐧𝐠 𝐢𝐧𝐟𝐞𝐜𝐭𝐢𝐨𝐧𝐬. 𝐈𝐭 𝐢𝐬 𝐚 𝐫𝐢𝐜𝐡 𝐬𝐨𝐮𝐫𝐜𝐞 𝐨𝐟 𝐦𝐚𝐠𝐧𝐞𝐬𝐢𝐮𝐦, 𝐟𝐢𝐛𝐞𝐫 𝐚𝐧𝐝 𝐩𝐨𝐭𝐚𝐬𝐬𝐢𝐮𝐦 𝐰𝐡𝐢𝐜𝐡 𝐡𝐞𝐥𝐩𝐬 𝐫𝐞𝐝𝐮𝐜𝐞 𝐛𝐥𝐨𝐨𝐝 𝐩𝐫𝐞𝐬𝐬𝐮𝐫𝐞. 𝐈𝐭 𝐢𝐬 𝐚𝐥𝐬𝐨 𝐜𝐨𝐧𝐬𝐢𝐝𝐞𝐫𝐞𝐝 𝐭𝐨 𝐥𝐨𝐰𝐞𝐫 𝐜𝐡𝐨𝐥𝐞𝐬𝐭𝐞𝐫𝐨𝐥 𝐥𝐞𝐯𝐞𝐥𝐬.                                    ',
                                    ImageUrl:'/assets/imagess/MatkiSabzi.png',
                                    Price:0
                                    },
                                    {
                                      id :32,
                                      name :'Oats Nuts Laddu',
                                      favourite :false,
                                      star :5,
                                      imageUrl :'/assets/imagess/Oats and Nuts Ladoo.jpg',
                                      cooktime : '20-25(min)',
                                      tags :['Desserts'],
                                      description : '𝐓𝐡𝐢𝐬 𝐡𝐞𝐚𝐥𝐭𝐡𝐲 𝐚𝐧𝐝 𝐭𝐚𝐬𝐭𝐲 𝐝𝐞𝐬𝐬𝐞𝐫𝐭 𝐢𝐬 𝐦𝐚𝐝𝐞 𝐰𝐢𝐭𝐡 𝐨𝐚𝐭𝐬 𝐚𝐧𝐝 𝐧𝐮𝐭𝐬 (𝐜𝐚𝐬𝐡𝐞𝐰𝐬, 𝐚𝐥𝐦𝐨𝐧𝐝𝐬, 𝐩𝐢𝐬𝐭𝐚𝐜𝐡𝐢𝐨𝐬) 𝐚𝐧𝐝 𝐢𝐬 𝐢𝐝𝐞𝐚𝐥 𝐟𝐨𝐫 𝐚𝐥𝐥 𝐭𝐡𝐞 𝐬𝐰𝐞𝐞𝐭 𝐥𝐨𝐯𝐞𝐫𝐬 𝐨𝐮𝐭 𝐭𝐡𝐞𝐫𝐞. 𝐈𝐟 𝐥𝐚𝐝𝐝𝐨𝐨𝐬 𝐚𝐫𝐞 𝐲𝐨𝐮𝐫 𝐟𝐚𝐯𝐨𝐫𝐢𝐭𝐞 𝐈𝐧𝐝𝐢𝐚𝐧 𝐦𝐢𝐭𝐡𝐚𝐢, 𝐭𝐡𝐞𝐧 𝐲𝐨𝐮 𝐜𝐚𝐧 𝐞𝐚𝐬𝐢𝐥𝐲 𝐦𝐚𝐤𝐞 𝐭𝐡𝐞𝐬𝐞 𝐝𝐞𝐥𝐢𝐜𝐢𝐨𝐮𝐬 𝐚𝐧𝐝 𝐧𝐮𝐭𝐫𝐢𝐭𝐢𝐨𝐮𝐬 𝐎𝐚𝐭𝐬 𝐍𝐮𝐭𝐬 𝐋𝐚𝐝𝐝𝐨𝐨𝐬 𝐚𝐭 𝐡𝐨𝐦𝐞. 𝐌𝐚𝐝𝐞 𝐰𝐢𝐭𝐡 𝐨𝐚𝐭𝐬 𝐚𝐧𝐝 𝐣𝐚𝐠𝐠𝐞𝐫𝐲, 𝐭𝐡𝐞𝐬𝐞 𝐥𝐚𝐝𝐝𝐨𝐨𝐬 𝐜𝐚𝐧 𝐚𝐥𝐬𝐨 𝐛𝐞 𝐞𝐚𝐭𝐞𝐧 𝐠𝐮𝐢𝐥𝐭-𝐟𝐫𝐞𝐞 𝐛𝐲 𝐚𝐥𝐥 𝐭𝐡𝐞 𝐰𝐞𝐢𝐠𝐡𝐭 𝐰𝐚𝐭𝐜𝐡𝐞𝐫𝐬 𝐨𝐮𝐭 𝐭𝐡𝐞𝐫𝐞.',
                                      ImageUrl:'/assets/imagess/OatsAndNutsLaddu.png',
                                      Price:0
                                      },
                                      {
                                        id :33,
                                        name :'Papaya Tarkari',
                                        favourite :false,
                                        star :5,
                                        imageUrl :'/assets/imagess/Papaya Tarkari (1).jpg',
                                        cooktime : '20-25(min)',
                                        tags :['Low Calorie Food'],
                                        description : 'Grated Raw Papaya Curry with Coconut, it will always be remembered as an ageless classic Bengali preparation. This is one of those traditional Bengali dishes which is cooked and enjoyed in several ways in both Bengals (West Bengal, India and Bangladesh). There are nothing called authentic recipe for the dish. But in most recipes grated raw papaya is cooked with crispy potato, desiccated coconut and flavoured with whole Garam Masala and other spices.',
                                        ImageUrl:'/assets/imagess/PapayaTarkari.png',
                                        Price:0
                                        },
                                        {
                                          id :34,
                                          name :'Skimmed Milk',
                                          favourite :false,
                                          star :5,
                                          imageUrl :'/assets/imagess/Skimmed Milk.jpg',
                                          cooktime : '20-25(min)',
                                          tags :['Dairy Free Products'],
                                          description : '𝐒𝐤𝐢𝐦𝐦𝐞𝐝 𝐦𝐢𝐥𝐤 (𝐁𝐫𝐢𝐭𝐢𝐬𝐡 𝐄𝐧𝐠𝐥𝐢𝐬𝐡), 𝐨𝐫 𝐬𝐤𝐢𝐦 𝐦𝐢𝐥𝐤 (𝐀𝐦𝐞𝐫𝐢𝐜𝐚𝐧 𝐄𝐧𝐠𝐥𝐢𝐬𝐡), 𝐢𝐬 𝐦𝐚𝐝𝐞 𝐰𝐡𝐞𝐧 𝐚𝐥𝐥 𝐭𝐡𝐞 𝐦𝐢𝐥𝐤𝐟𝐚𝐭 𝐢𝐬 𝐫𝐞𝐦𝐨𝐯𝐞𝐝 𝐟𝐫𝐨𝐦 𝐰𝐡𝐨𝐥𝐞 𝐦𝐢𝐥𝐤. 𝐈𝐭 𝐭𝐞𝐧𝐝𝐬 𝐭𝐨 𝐜𝐨𝐧𝐭𝐚𝐢𝐧 𝐚𝐫𝐨𝐮𝐧𝐝 𝟎.𝟏% 𝐟𝐚𝐭.',
                                          ImageUrl:'/assets/imagess/SkimmedMilk.png',
                                          Price:0
                                          },
                                          {
                                            id :35,
                                            name :'Turkey Soup',
                                            favourite :false,
                                            star :5,
                                            imageUrl :'/assets/imagess/Turkey Soup.jpg',
                                            cooktime : '20-25(min)',
                                            tags :['Soups'],
                                            description : '𝐀 𝐠𝐨𝐨𝐝 𝐡𝐨𝐦𝐞𝐦𝐚𝐝𝐞 𝐓𝐮𝐫𝐤𝐞𝐲 𝐒𝐨𝐮𝐩 𝐫𝐞𝐜𝐢𝐩𝐞 𝐚 𝐠𝐨 𝐭𝐨 𝐚𝐟𝐭𝐞𝐫 𝐡𝐨𝐥𝐢𝐝𝐚𝐲 𝐦𝐞𝐚𝐥𝐬! 𝐓𝐞𝐧𝐝𝐞𝐫 𝐜𝐡𝐮𝐧𝐤𝐬 𝐨𝐟 𝐥𝐞𝐟𝐭𝐨𝐯𝐞𝐫 𝐭𝐮𝐫𝐤𝐞𝐲, 𝐜𝐚𝐫𝐫𝐨𝐭𝐬, 𝐨𝐧𝐢𝐨𝐧𝐬, 𝐜𝐞𝐥𝐞𝐫𝐲, 𝐚𝐧𝐝 𝐩𝐚𝐬𝐭𝐚 𝐧𝐨𝐨𝐝𝐥𝐞𝐬 𝐚𝐫𝐞 𝐬𝐢𝐦𝐦𝐞𝐫𝐞𝐝 𝐢𝐧 𝐚 𝐝𝐞𝐥𝐢𝐜𝐢𝐨𝐮𝐬 𝐡𝐨𝐦𝐞𝐦𝐚𝐝𝐞 𝐭𝐮𝐫𝐤𝐞𝐲 𝐬𝐭𝐨𝐜𝐤. 𝐒𝐞𝐫𝐯𝐞 𝐢𝐭 𝐚𝐥𝐨𝐧𝐠𝐬𝐢𝐝𝐞 𝐬𝐨𝐦𝐞 𝐝𝐢𝐧𝐧𝐞𝐫 𝐫𝐨𝐥𝐥𝐬 𝐚𝐧𝐝 𝐭𝐫𝐚𝐧𝐬𝐜𝐞𝐧𝐝 𝐢𝐧𝐭𝐨 𝐜𝐨𝐦𝐟𝐨𝐫𝐭 𝐟𝐨𝐨𝐝 𝐡𝐞𝐚𝐯𝐞𝐧!',
                                            ImageUrl:'/assets/imagess/TurkeySoup.png',
                                            Price:0
                                            },
                                            {
                                              id :36,
                                              name :'Tomato Methi Pulao',
                                              favourite :false,
                                              star :5,
                                              imageUrl :'/assets/imagess/Tomato Methi Pulao.jpg',
                                              cooktime : '20-25(min)',
                                              tags :['Rice'],
                                              description : '𝐓𝐨𝐦𝐚𝐭𝐨 𝐌𝐞𝐭𝐡𝐢 𝐑𝐢𝐜𝐞 𝐑𝐞𝐜𝐢𝐩𝐞 𝐢𝐬 𝐚 𝐩𝐨𝐰𝐞𝐫 𝐩𝐚𝐜𝐤𝐞𝐝 𝐝𝐞𝐥𝐢𝐜𝐢𝐨𝐮𝐬 𝐫𝐢𝐜𝐞 𝐝𝐢𝐬𝐡. 𝐌𝐞𝐭𝐡𝐢 𝐨𝐫 𝐟𝐞𝐧𝐮𝐠𝐫𝐞𝐞𝐤 𝐥𝐞𝐚𝐯𝐞𝐬 𝐚𝐫𝐞 𝐚 𝐩𝐨𝐰𝐞𝐫𝐡𝐨𝐮𝐬𝐞 𝐨𝐟 𝐢𝐫𝐨𝐧 𝐚𝐧𝐝 𝐕𝐢𝐭𝐚𝐦𝐢𝐧 𝐂. 𝐒𝐢𝐧𝐜𝐞 𝐢𝐭 𝐢𝐬 𝐚𝐥𝐬𝐨 𝐫𝐢𝐜𝐡 𝐢𝐧 𝐝𝐢𝐞𝐭𝐚𝐫𝐲 𝐟𝐢𝐛𝐫𝐞𝐬, 𝐢𝐭 𝐢𝐬 𝐛𝐞𝐧𝐞𝐟𝐢𝐜𝐢𝐚𝐥 𝐟𝐨𝐫 𝐜𝐨𝐧𝐭𝐫𝐨𝐥𝐥𝐢𝐧𝐠 𝐭𝐡𝐞 𝐬𝐮𝐠𝐚𝐫 𝐥𝐞𝐯𝐞𝐥 𝐚𝐧𝐝 𝐜𝐡𝐨𝐥𝐞𝐬𝐭𝐞𝐫𝐨𝐥 𝐢𝐧 𝐭𝐡𝐞 𝐛𝐨𝐝𝐲. 𝐈𝐭 𝐮𝐬𝐞𝐬 𝐦𝐢𝐥𝐝 𝐬𝐩𝐢𝐜𝐞𝐬 𝐦𝐚𝐤𝐢𝐧𝐠 𝐢𝐭 𝐚 𝐩𝐞𝐫𝐟𝐞𝐜𝐭 𝐤𝐢𝐝𝐬 𝐥𝐮𝐧𝐜𝐡 𝐛𝐨𝐱 𝐫𝐞𝐜𝐢𝐩𝐞.𝐒𝐞𝐫𝐯𝐞 𝐭𝐡𝐞 𝐓𝐨𝐦𝐚𝐭𝐨 𝐌𝐞𝐭𝐡𝐢 𝐑𝐢𝐜𝐞 𝐑𝐞𝐜𝐢𝐩𝐞 𝐚𝐥𝐨𝐧𝐠 𝐰𝐢𝐭𝐡 𝐓𝐨𝐦𝐚𝐭𝐨 𝐎𝐧𝐢𝐨𝐧 𝐂𝐮𝐜𝐮𝐦𝐛𝐞𝐫 𝐫𝐚𝐢𝐭𝐚 𝐚𝐧𝐝 𝐫𝐨𝐚𝐬𝐭𝐞𝐝 𝐩𝐚𝐩𝐚𝐝 𝐟𝐨𝐫 𝐤𝐢𝐝𝐬 𝐥𝐮𝐧𝐜𝐡 𝐛𝐨𝐱 𝐨𝐫 𝐟𝐨𝐫 𝐚 𝐥𝐢𝐠𝐡𝐭 𝐰𝐞𝐞𝐤𝐧𝐢𝐠𝐡𝐭 𝐝𝐢𝐧𝐧𝐞𝐫.',
                                              ImageUrl:'/assets/imagess/TomatoMethiPulao (1).png',
                                              Price:0
                                              },


    
  ]

}
}
import { Injectable } from '@angular/core';
import {Cart} from "../shared/models/Cart"
import {CartItem} from "src/app/shared/models/cartItem";
import {Foods} from "src/app/shared/models/food";


@Injectable({
  providedIn: 'root'
})
export class CartService {
private cart:Cart=new Cart();
  addToCart(food:Foods):void{
  this.cart.items.push(new CartItem(food));
  }

  removeFromCart(foodId:number):void{
    this.cart.items = this.cart.items.filter((item => item.food.id != foodId))
  }
  getCart() :Cart{
    return this.cart;
  }
}

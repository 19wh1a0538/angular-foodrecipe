import { Component, Input, OnInit } from '@angular/core';
import { FoodsService } from '../services/foods/foods.service';
import { Tag } from '../shared/models/tag';
@Component({
selector: 'app-tags',
templateUrl: './tags.component.html',
styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {
tags:Tag[] = [] ;

constructor( private ps:FoodsService) { }
ngOnInit(): void {
this.tags=this.ps.getAllTag();
}
}
